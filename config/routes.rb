Rails.application.routes.draw do
  resources :benchmarks, only: :none do
    collection do
      get :simple
    end
  end

  namespace :api do
    namespace :v1 do
      post '/player_data', to: 'players#player_data'
    end
  end
end