describe Player do
  let(:player) { Player.create(social_id: 'a') }
  let(:other_player) { Player.create(social_id: 'b') }
  let(:the_friend) { Player.create(social_id: 'c') }
  let(:puzzle_1) { Puzzle.create(simple_id: 1) }
  let(:puzzle_2) { Puzzle.create(simple_id: 2) }
  let(:puzzle_3) { Puzzle.create(simple_id: 3) }

  before do
    Played.create(from_node: player, to_node: puzzle_1, score: 30, star: 2)
    Played.create(from_node: player, to_node: puzzle_2, score: 20, star: 2)
    Played.create(from_node: player, to_node: puzzle_3, score: 30, star: 3)

    Played.create(from_node: other_player, to_node: puzzle_1, score: 50, star: 3)
    Played.create(from_node: other_player, to_node: puzzle_2, score: 40, star: 3)
    Played.create(from_node: other_player, to_node: puzzle_3, score: 10, star: 3)

    Played.create(from_node: the_friend, to_node: puzzle_1, score: 40, star: 1)
    Played.create(from_node: the_friend, to_node: puzzle_2, score: 50, star: 2)
    Played.create(from_node: the_friend, to_node: puzzle_3, score: 10, star: 3)
  end

  describe "validations" do
    context 'with unique social_id' do
      before do
        player.update_attributes(social_id: '1')
      end

      it 'is valid when social_id does not exists' do
        other_player.update_attributes(social_id: '2')
        expect(other_player.valid?).to eq true
      end

      it 'is invalid when social_id already exist' do
        other_player.update_attributes(social_id: '1')
        expect(other_player.valid?).to eq false
      end
    end
  end

  describe "associations" do
    it "should have many friends" do
      t = Player.reflect_on_association(:friends)
      expect(t.macro).to eq :has_many
      expect(t.type).to eq :FRIENDS_WITH
    end

    it "should have unique relationship with a friend" do
      player.friends << other_player
      player.friends << other_player

      relationship = player.query_as(:p)
      .match('(p)-[r:FRIENDS_WITH]-(f:Player {social_id: "b"})')
      .pluck(:r)

      expect(relationship.count).to eq 1
    end

    it "should have many puzzles" do
      t = Player.reflect_on_association(:puzzles)
      expect(t.macro).to eq :has_many
      expect(t.type).to eq :PLAYED
    end

    it "should have unique relationship with a puzzle" do
      player.puzzles << puzzle_1
      player.puzzles << puzzle_1

      relationship = player.query_as(:p)
      .match('(p)-[r:PLAYED]-(z:Puzzle {simple_id: 1})')
      .pluck(:r)

      expect(relationship.count).to eq 1
    end
  end

  describe '#puzzle_data' do
    let(:puzzle_data) { player.puzzle_data }
    let(:puzzle_ids) { puzzle_data.first }
    let(:puzzle_scores) { puzzle_data.second }
    let(:puzzle_stars) { puzzle_data.third }

    it 'returns puzzles ids played the player' do
      expect(puzzle_ids).to match [1,2,3]
    end

    it 'returns scores for puzzles played the player' do
      expect(puzzle_scores).to match [30,20,30]
    end

    it 'returns stars for puzzles played the player' do
      expect(puzzle_stars).to match [2,2,3]
    end
  end

  describe '#high_scores' do
    let(:high_scores) { player.high_scores }
    let(:high_scores_puzzles) { high_scores.first }
    let(:high_scores_leaders) { high_scores.second }
    let(:high_scores_scores) { high_scores.third }

    before do
      player.friends << other_player
      player.friends << the_friend
    end

    it 'returns high scores for puzzles played by the player' do
      expect(high_scores_puzzles).to match [1,2,3]
      expect(high_scores_leaders).to match ['b','c','b']
      expect(high_scores_scores).to match [50,50,10]
    end
  end
end
