describe Puzzle do
  let(:puzzle) { Puzzle.create(simple_id: 1) }
  let(:other_puzzle) { Puzzle.create(simple_id: 2) }

  describe "validations" do
    it 'is valid when simple_id is an Integer' do
      puzzle.update_attributes(simple_id: 5)
      expect(puzzle.valid?).to eq true
    end

    it 'is invalid when simple_id is not an Integer' do
      puzzle.update_attributes(simple_id: 'abc')
      expect(puzzle.valid?).to eq false
    end

    context 'with unique simple_id' do
      it 'is valid when simple_id does not exists' do
        other_puzzle.update_attributes(simple_id: 3)
        expect(other_puzzle.valid?).to eq true
      end

      it 'is invalid when simple_id already exist' do
        puzzle.update_attributes(simple_id: 1)
        other_puzzle.update_attributes(simple_id: 1)
        expect(other_puzzle.valid?).to eq false
      end
    end
  end
end
