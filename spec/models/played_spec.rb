describe Played do
  let(:player) { Player.create(social_id: 'a') }
  let(:puzzle) { Puzzle.create(simple_id: 1) }
  let(:other_player) { Player.create(social_id: 'b') }
  let(:other_puzzle) { Puzzle.create(simple_id: 2) }

  let(:played) { Played.create(from_node: player, to_node: puzzle) }
  let(:other_played) { Played.create(from_node: other_player, to_node: other_puzzle) }

  describe "validations" do
    it 'is valid when score is within range' do
      played.update_attributes(score: 8000)
      expect(played.valid?).to eq true
    end

    it 'is invalid when score is outside range' do
      played.update_attributes(score: 11000)
      expect(played.valid?).to eq false
    end

    it 'is valid when star is within range' do
      played.update_attributes(star: 2)
      expect(played.valid?).to eq true
    end

    it 'is invalid when star is outside range' do
      played.update_attributes(star: 5)
      expect(played.valid?).to eq false
    end
  end

  it 'retains unique association between player and puzzle' do
    same_played = Played.create(from_node: player, to_node: puzzle)
    expect(played.id).to eq same_played.id
  end

  it 'retains score when updated with lower score' do
    played.update_attributes(score: 10000)
    played.update_attributes(score: 5000)

    expect(played.score).to eq 10000
  end

  it 'retains star when updated with lower star' do
    played.update_attributes(star: 3)
    played.update_attributes(star: 1)

    expect(played.star).to eq 3
  end
end
