describe PlayerDataParam do

	describe "valid?" do

		subject { PlayerDataParam.new(params).valid? }

		context "when social_id is not present" do
			let(:params) do
				{
					"some" => "data"
				}
			end

			it { is_expected.to eq false }
		end

		context "when social_id is present" do
			let(:params) do
				{
					"player" => {
						"social_id" => "some-id"	
					}
				}
			end

			it { is_expected.to eq true }
		end

		context "when puzzle data is not matching in length" do
			let(:params) do
				{
					"player" => {
						"social_id" => "some-id",
						"puzzle_data" => {
							"puzzles" => [1,2,3],
							"scores" => [10,20],
							"stars" => [1]
						}
					}
				}
			end	

			it { is_expected.to eq false }
		end

		context "when puzzle data is matching in length" do
			let(:params) do
				{
					"player" => {
						"social_id" => "some-id",
						"puzzle_data" => {
							"puzzles" => [1,2,3],
							"scores" => [10,20,30],
							"stars" => [1,2,3]
						}
					}
				}
			end	

			it { is_expected.to eq true }
		end

	end
end