describe PlayerData do
  let(:base_params) do
    {
      'player' => {
        'email' => 'some-email@game.com',
        'name' => 'Bob',
        'social_id' => 'social-id-0',
        'puzzle_data' => {
          'puzzles' => [1, 2, 3, 6],
          'scores' => [304, 2001, 2, 908],
          'stars' => [2, 3, 1, 3]
        },
        'friends' => [
          'social-id-1',
          'social-id-2',
          'social-id-3'
        ]
      }
    }
  end
  let(:player_data_param) { PlayerDataParam.new(params) }

  subject { described_class.new(player_data_param) }

  describe '#upsert' do
    let(:player) { Player.find_by(social_id: 'social-id-0') }

    context 'when all params are valid and present' do
      let(:params) { base_params }
      before do
        subject.upsert
      end

      it 'updates the player' do
        expect(player.name).to eq 'Bob'
        expect(player.email).to eq 'some-email@game.com'
      end

      it 'updates the puzzle played by the player' do
        puzzles, scores, stars = [], [], []
        player.puzzles.each_with_rel do |puzzle, rel|
          puzzles << puzzle.simple_id
          scores << rel.score
          stars << rel.star
        end

        expect(puzzles).to match [1, 2, 3, 6]
        expect(scores).to match [304, 2001, 2, 908]
        expect(stars).to match [2, 3, 1, 3]
      end

      it 'updates the player association with other players' do
        friends = player.friends.map(&:social_id)
        expect(friends).to match ['social-id-1', 'social-id-2', 'social-id-3']
      end
    end

    context 'when email param is missing' do
      let(:params) do
        base_params['player'].delete('email')
        base_params
      end

      it 'does not update player''s email' do
        expect(subject.player).to receive(:update_attributes).with({name: 'Bob'})

        subject.upsert
      end
    end

    context 'when name param is missing' do
      let(:params) do
        base_params['player'].delete('name')
        base_params
      end

      it 'does not update player''s name' do
        expect(subject.player).to receive(:update_attributes).with({email: 'some-email@game.com'})

        subject.upsert
      end
    end

    context 'when one or more puzzle params are invalid' do
      let(:params) do
        base_params['player']['puzzle_data']['stars'][0] = 99
        base_params
      end

      it 'raise exception' do
        expect{subject.upsert}.to raise_exception.with_message(/star.*is not included in the list/)
      end
    end

  end
end
