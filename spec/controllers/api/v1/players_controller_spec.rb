module Api::V1
  describe PlayersController do

    describe "POST player_data" do
      let(:params) { Hash[:data, "something"] }
      let(:player_data_param) { double(:player_data_param) }
      let(:parsed_body) { JSON.parse(response.body) }
      let(:player_data) { double(:player_data) }
      let(:result) { Hash[:key, :value] }
      let(:errors) { double(messages: 'real-bad-errors') }
      let(:presenter) { double(:presenter) }

      before do
        allow(PlayerDataParam).to receive(:new) { player_data_param }
        allow(player_data_param).to receive(:valid?) { valid_param }
        allow(player_data_param).to receive(:errors) { errors }
        allow(PlayerData).to receive(:new).with(player_data_param) { player_data }
        allow(player_data).to receive(:upsert) { result }
        allow(player_data).to receive(:player)
        allow(PlayerDataPresenter).to receive(:new) { presenter }
        allow(presenter).to receive(:to_hash) { result }
      end

      context "when params is valid" do
        let(:valid_param) { true }

        it "returns serialized player data" do
          post :player_data, params

          expect(response.code).to eq "200"
          expect(parsed_body).to eq result.as_json
        end
      end

      context "when params is invalid" do
        let(:valid_param) { false }

        it "returns serialized errors object" do
          post :player_data, params

          expect(response.code).to eq "500"
          expect(parsed_body).to eq({"error"=>"internal-server-error", "exception"=>"Exception : real-bad-errors"})
        end
      end
    end
  end
end
