describe PlayerDataPresenter do
  let!(:player) { Player.create(social_id: 'a') }
  let!(:other_player) { Player.create(social_id: 'b') }

  let!(:puzzle) { Puzzle.create(simple_id: 1) }
  let!(:other_puzzle) { Puzzle.create(simple_id: 2) }

  let!(:played_1) { Played.create(from_node: player, to_node: puzzle, score: 5000, star: 2) }
  let!(:played_1) { Played.create(from_node: player, to_node: other_puzzle, score: 1000, star: 1) }
  let!(:other_played_1) { Played.create(from_node: other_player, to_node: puzzle, score: 1000, star: 1) }
  let!(:other_played_2) { Played.create(from_node: other_player, to_node: other_puzzle, score: 8000, star: 3) }

  before do
    player.friends << other_player
  end

  describe '#to_hash' do
    let(:presenter) { PlayerDataPresenter.new(player) }
    let(:expected_hash) do
      {
        :player => {
          :puzzle_data => {
            :puzzles => [2],
            :scores => [1000],
            :stars => [1]
          },
          :friends => [
            {
              :social_id => "b",
              :puzzle_data => {
                :puzzles => [1, 2],
                :scores => [1000, 8000],
                :stars => [1, 3]
              }
            }
          ],
          :high_scores => {
            :puzzles => [2],
            :leaders => ["b"],
            :scores => [8000]
          }
        }
      }
    end

    it 'returns representation of player data' do
      expect(presenter.to_hash).to eq expected_hash
    end
  end
end
