describe "routes for Players", :type => :routing do
  it "routes /api/v1/player_data to the players controller" do
    expect(post("/api/v1/player_data")).to route_to("api/v1/players#player_data")
    expect(get("/api/v1/player_data")).not_to be_routable
  end
end