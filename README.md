# Dots Nukem: Forever - API

Backend service for mobile game Dots Nukem: Forever

# Setup
```bash
bundle install
```

# Run Locally
```bash
rake neo4j:start[development]
bundle exec puma -C config/puma.rb
```

# Run Tests
```bash
rake neo4j:start[test]
rspec
```