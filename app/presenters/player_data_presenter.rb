class PlayerDataPresenter
  attr_reader :player

  def initialize(player)
    @player = player
  end

  def to_hash
    node = base_hash

    player_node = node[:player]
    player_node[:puzzle_data] = puzzle_data_hash(*player.puzzle_data)
    player_node[:friends] = friends_hash(player.friends)
    player_node[:high_scores] = high_scores_hash(*player.high_scores)

    node
  end

  private

  def base_hash
    {
      player: {
        puzzle_data: {},
        friends: [],
        high_scores: {}
      }
    }
  end

  def puzzle_data_hash(puzzles, scores, stars)
    {
      puzzles: puzzles,
      scores: scores,
      stars: stars
    }
  end

  def friends_hash(friends)
    friends.map do |friend|
      {
        social_id: friend.social_id,
        puzzle_data: puzzle_data_hash(*friend.puzzle_data)
      }
    end
  end

  def high_scores_hash(puzzles, leaders, scores)
    {
      puzzles: puzzles,
      leaders: leaders,
      scores: scores
    }
  end
end
