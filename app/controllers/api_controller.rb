class ApiController < ActionController::Metal
  abstract!

  include AbstractController::Callbacks
  include ActionController::RackDelegation
  include ActionController::StrongParameters
  include ActionController::Rescue

  rescue_from Exception, with: :error

  private

  def render(options={})
    self.status = options[:status] || 200
    self.content_type = 'application/json'
    body = Oj.dump(options[:json], mode: :compat)
    self.headers['Content-Length'] = body.bytesize.to_s
    self.response_body = body
  end

  ActiveSupport.run_load_hooks(:action_controller, self)

  protected

  def error(e)
    if request.env["ORIGINAL_FULLPATH"] =~ /^\/api/ || Rails.env.test?
      error_info = {
        :error => "internal-server-error",
        :exception => "#{e.class.name} : #{e.message}",
      }
      error_info[:trace] = e.backtrace[0,10] if Rails.env.development?
      render json: error_info, status: :internal_server_error
    else
      raise e
    end
  end
end
