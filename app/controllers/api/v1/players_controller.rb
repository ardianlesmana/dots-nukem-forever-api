module Api::V1
  class PlayersController < ApiController

    def player_data
      player_data_param = PlayerDataParam.new(params)

      unless player_data_param.valid?
        raise Exception.new(player_data_param.errors.messages)
      end
      
      player_data = PlayerData.new(player_data_param)
      player_data.upsert

      render json: PlayerDataPresenter.new(player_data.player).to_hash
    end

  end
end
