class Played
  include Neo4j::ActiveRel
  include Neo4j::Timestamps

  before_update :ensure_highest_score_stays
  before_update :ensure_highest_star_stays

  creates_unique

  from_class :Player
  to_class :Puzzle
  type :PLAYED

  property :score, type: Integer, default: 0
  property :star, type: Integer, default: 0

  validates :score, inclusion: { in: 0..10000 }, numericality: { only_integer: true }
  validates :star, inclusion: { in: 0..3 }, numericality: { only_integer: true }

  private

  def ensure_highest_score_stays
    self.score = score_changed? ? score_change.compact.max : score
  end

  def ensure_highest_star_stays
    self.star = star_changed? ? star_change.compact.max : star
  end
end
