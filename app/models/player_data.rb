class PlayerData
  include ActiveModel::Validations

  attr_reader :params, :player

  def initialize(player_data_param)
    @params = player_data_param
    @player = Player.find_or_create_by(social_id: params.social_id)
  end

  def upsert
    update_player
    update_puzzle_data(player)
    update_friends_relationship(player)
  end

  private

  def update_player
    player_params = {}
    player_params.merge!({name: params.name}) unless params.name.nil?
    player_params.merge!({email: params.email}) unless params.email.nil?

    player.update_attributes(player_params)

    raise Exception.new(player.errors.messages) unless player.valid?
  end

  def update_puzzle_data(player)
    params.puzzles_params.each do |p|
      puzzle = Puzzle.find_or_create_by(simple_id: p[:simple_id])
      if puzzle.present?
        player.puzzles.find_or_create_by(simple_id: puzzle.simple_id)

        rel = played(player, puzzle)
        rel.update(score: p[:score], star: p[:star])

        raise Exception.new(rel.errors.messages) unless rel.valid?
      end
    end
  end

  def played(player, puzzle)
    player.query_as(:p)
    .match("p-[rel:PLAYED]->(z:Puzzle {simple_id: {simple_id}})")
    .params(simple_id: puzzle.simple_id)
    .pluck(:rel)
    .first
  end

  def update_friends_relationship(player)
    params.friends_ids.each do |friend_social_id|
      friend_param = { social_id: friend_social_id }

      # Upsert friend
      friend = Player.find_or_create_by(friend_param)

      # Associate with friend
      player.friends.find_or_create_by(friend_param)

      raise Exception.new(player.errors.messages) unless player.valid?
    end
  end
end
