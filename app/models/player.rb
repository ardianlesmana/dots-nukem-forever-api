class Player
  include Neo4j::ActiveNode
  include Neo4j::Timestamps

  property :social_id, constraint: :unique
  property :email
  property :name

  validates :social_id, presence: true, uniqueness: true

  has_many :out, :friends, type: :FRIENDS_WITH, model_class: :Player, unique: :all
  has_many :out, :puzzles, rel_class: :Played, model_class: :Puzzle

  def puzzle_data
    puzzles, scores, stars = [], [], []
    self.puzzles.each_with_rel do |puzzle, rel|
      puzzles << puzzle.simple_id
      scores << rel.score
      stars << rel.star
    end

    return puzzles, scores, stars
  end

  def high_scores
    puzzles, leaders, scores = [], [], []
    self.puzzles.each do |puzzle|
      r = puzzle.query_as(:z)
      .match('(z)-[r:PLAYED]-(f:Player)')
      .match('(f)-[:FRIENDS_WITH]-(p:Player {social_id: {social_id}})')
      .params(social_id: self.social_id)
      .order(r: {score: :desc}).limit(1)
      .return(f: [:social_id], r: [:score])
      .first

      next if r.nil?

      puzzles << puzzle.simple_id
      leaders << r["f.social_id"]
      scores << r["r.score"]
    end

    return puzzles, leaders, scores
  end
end
