class Puzzle
  include Neo4j::ActiveNode
  include Neo4j::Timestamps

  property :simple_id, type: Integer, constraint: :unique

  validates :simple_id, numericality: { only_integer: true }, presence: true, uniqueness: true
end
