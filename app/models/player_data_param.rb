class PlayerDataParam
  include ActiveModel::Validations

  attr_reader :social_id, :name, :email, :friends_ids

  validates_presence_of :social_id
  validate :validate_puzzle_data

  def initialize(params={})
    player = params.fetch('player', {})
    @social_id = player.fetch('social_id', nil)
    @name = player.fetch('name', nil)
    @email = player.fetch('email', nil)

    puzzle_data = player.fetch('puzzle_data', {})
    @puzzles = puzzle_data.fetch('puzzles', [])
    @scores = puzzle_data.fetch('scores', [])
    @stars = puzzle_data.fetch('stars', [])

    @friends_ids = player.fetch('friends', [])
  end

  def puzzles_params
    @puzzles.each_with_index.map do |puzzle_simple_id, index|
      {
        simple_id: puzzle_simple_id,
        score: @scores[index],
        star: @stars[index]
      }
    end
  end

  private

  def validate_puzzle_data
    return if [@scores.length, @stars.length].all? { |l| l == @puzzles.length }
    errors.add(:puzzles, "puzzle data arrays are not the same size")
  end

end
